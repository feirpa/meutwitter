package br.com.feirpa.meutwitter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth minhaAuth;
    private FirebaseAuth.AuthStateListener minhaAuthListener;

    private EditText txtNome, txtEmail, txtSenha;
    private Button btnLogin, btnSingUp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNome = (EditText) findViewById(R.id.txtNome);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtSenha = (EditText) findViewById(R.id.txtSenha);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnSingUp = (Button) findViewById(R.id.btnSingUp);

        minhaAuth = FirebaseAuth.getInstance();

        minhaAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d("meuLog", "Usuario conectado: " + user.getUid());
                    Intent i = new Intent(getApplicationContext(), SegundaTela.class);
                    startActivity(i);
                } else {
                    Log.d("meuLog", "Sem usuarios conectados.");
                }
            }
        };

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicaLogin(v);
            }
        });

        btnSingUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicaCriarUsuario(v);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        minhaAuth.addAuthStateListener(minhaAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (minhaAuthListener != null) {
            minhaAuth.removeAuthStateListener(minhaAuthListener);
        }
    }

    private void clicaLogin(View view) {
        minhaAuth.signInWithEmailAndPassword(txtEmail.getText().toString(), txtSenha.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Log.d("meuLog", "Falha na autenticacao");
                        }
                    }
                });
    }

    private void clicaCriarUsuario(View view) {
        minhaAuth.createUserWithEmailAndPassword(txtEmail.getText().toString(), txtSenha.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Log.d("meuLog", "Falha na criacao de conta");
                        } else {
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference ref = database.getReference("users")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
                            ref.child("nome").setValue(txtNome.getText().toString());
                            ref.child("uid").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        }
                    }
                });
    }
}
